export interface OrderEvaluation {
    productName: string;
    clientName: string;
    // the id of the order - this is the id of the order inside the OpenCRX System
    id: string;
    // the id of the internal OrderBonus model - equals null, if the kpi was not yet evaluated
    bonusId: string | null;
    clientRanking: number;
    quantity: number;
    bonus: number | null;
    comment: string | null;
}

export interface Salesman {
    employeeId: string;
    fullName: string;
    code: string;
    unit: string;
}

export interface SocialEvaluation {
    kpi: string;
    // the id of the internal KPI model.
    id: string;
    // the id of the internal KPIBonus model - equals null, if the kpi was not yet evaluated
    bonusId: string | null;
    targetValue: string;
    actualValue: string;
    bonus: number | null;
    comment: string | null;
}

export class User {
    username: string;
    id: string;
    department: string;
    password: string;
}

export enum Department {
    HR = "HR",
    CEO = "CEO",
    Sales = "Sales",
}

export interface EvaluationRecord {
    _id: string;
    salesmanId: string;
    year: string;
    remarks: string;
    bonus: number;
    department: string;
    orderBonus: [];
    kpiBonus: [];
    orderEvaluation: OrderEvaluation[];
    socialEvaluation: SocialEvaluation[];
    isApprovedByCEO: boolean;
    isApprovedBySalesman: boolean;
    isApprovedByHR: boolean;
}

export interface KPI {
    _id: string;
    targetValue: string;
    kpi: string;
}
