import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "./services/auth/authentication.service";
import TokenService from "./services/auth/token.service";
import { Router } from "@angular/router";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
    constructor(
        protected readonly router: Router,
        protected readonly tokenService: TokenService,
        public readonly authService: AuthenticationService,
    ) {}

    redirectToHome() {
        return this.router.navigate(["/home"]);
    }

    ngOnInit(): void {
        // check if we need to redirect the user.
        // main entry point into the application
        if (this.tokenService.getAccessToken()) {
            // we can try to sync the user
            this.authService.sync().subscribe(
                () => {
                    if (this.router.url === "/login") {
                        return this.router.navigate(["/home"]);
                    }
                },
                () => {
                    return this.router.navigate(["/login"]);
                },
            );
        }
        // there is no access token stored.
        // trying a sync will always fail.
        // just redirect to login route.
    }

    logout() {
        this.authService.logout();
        this.router.navigate([`/login`]);
    }
}
