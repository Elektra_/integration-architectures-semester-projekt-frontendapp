import { TestBed } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { RouterTestingModule } from "@angular/router/testing";
import _PersistentStorageMock from "./mocks/_PersistentStorageMock";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { AntdModule } from "./antd/antd.module";

describe("AppComponent", () => {
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule, AntdModule],
            providers: [{ provide: "PersistentStorage", useClass: _PersistentStorageMock }],
            declarations: [AppComponent],
        }).compileComponents();
    });

    it("should create the app", () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });
});
