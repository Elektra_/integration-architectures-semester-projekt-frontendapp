import { Routes, RouterModule } from "@angular/router";

import { LoginComponent } from "./pages/login/login.component";
import { HomeComponent } from "./pages/home/home.component";
import { EvaluationRecordsComponent } from "./pages/evaluation-records/evaluation-records.component";
import { EditEvaluationRecordComponent } from "./pages/edit-evaluation-record/edit-evaluation-record.component";
import { RegisterComponent } from "./pages/register/register.component";

const routes: Routes = [
    { path: "login", component: LoginComponent },
    { path: "register", component: RegisterComponent },
    { path: "home", component: HomeComponent },
    { path: "evaluation-records/:sid", component: EvaluationRecordsComponent },
    { path: "evaluation-records/:sid/edit/:evaluationRecordId", component: EditEvaluationRecordComponent },

    // otherwise redirect to home
    { path: "**", redirectTo: "login" },
];

export const appRoutingModule = RouterModule.forRoot(routes);
