export default class BonusCalculator {
    static socialEvaluationBonus(actualValue: number, targetValue: number): number {
        if (actualValue > targetValue) {
            return 100;
        } else if (actualValue == targetValue) {
            return 50;
        } else if (Math.round(actualValue / targetValue) >= 0.75) {
            return 20;
        } else if (Math.round(actualValue / targetValue) >= 0.5) {
            return 10;
        } else if (Math.round(actualValue / targetValue) >= 0.25) {
            return 5;
        } else {
            return 0;
        }
    }
}
