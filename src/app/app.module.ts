import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./pages/login/login.component";
import { EvaluationRecordsComponent } from "./pages/evaluation-records/evaluation-records.component";
import { HomeComponent } from "./pages/home/home.component";
import { appRoutingModule } from "./app.routing";
import { EditEvaluationRecordComponent } from "./pages/edit-evaluation-record/edit-evaluation-record.component";
import { HttpRequestInterceptor } from "./services/auth/http-request-interceptor.service";
import { RegisterComponent } from "./pages/register/register.component";
import { NZ_I18N } from "ng-zorro-antd/i18n";
import { en_US } from "ng-zorro-antd/i18n";
import { registerLocaleData } from "@angular/common";
import en from "@angular/common/locales/en";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AntdModule } from "./antd/antd.module";

registerLocaleData(en);

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        appRoutingModule,
        HttpClientModule,
        FormsModule,
        BrowserAnimationsModule,
        AntdModule,
    ],
    declarations: [
        AppComponent,
        EvaluationRecordsComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        EditEvaluationRecordComponent,
        RegisterComponent,
    ],
    providers: [
        {
            provide: "PersistentStorage",
            useValue: localStorage,
        },
        { provide: HTTP_INTERCEPTORS, useClass: HttpRequestInterceptor, multi: true },
        { provide: NZ_I18N, useValue: en_US },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {}
