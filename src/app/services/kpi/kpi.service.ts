import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { KPI } from "../../interfaces";
import { environment } from "../../../environments/environment";

@Injectable({
    providedIn: "root",
})
export class KpiService {
    constructor(protected httpClient: HttpClient) {}

    getAll() {
        return this.httpClient.get<KPI[]>(`${environment.backendApiUrl}/kpi`);
    }

    delete(id: string) {
        return this.httpClient.delete<void>(`${environment.backendApiUrl}/kpi/${id}`);
    }

    create(kpi: Partial<KPI>) {
        return this.httpClient.post<KPI>(`${environment.backendApiUrl}/kpi`, kpi);
    }
}
