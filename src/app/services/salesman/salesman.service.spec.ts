import { TestBed } from "@angular/core/testing";

import { SalesmanService } from "./salesman.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("SalesmanService", () => {
    let service: SalesmanService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });
        service = TestBed.inject(SalesmanService);
    });

    it("should be created", () => {
        expect(service).toBeTruthy();
    });
});
