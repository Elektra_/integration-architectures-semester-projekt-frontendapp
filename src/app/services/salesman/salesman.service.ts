import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { Salesman } from "../../interfaces";

@Injectable({
    providedIn: "root",
})
export class SalesmanService {
    selectedSalesman: Salesman;
    constructor(private readonly httpClient: HttpClient) {}

    getSalesman(): Observable<Salesman[]> {
        return this.httpClient.get<Salesman[]>(`${environment.backendApiUrl}/salesman`);
    }

    showSalesman(sid: string): Observable<Salesman> {
        return this.httpClient.get<Salesman>(`${environment.backendApiUrl}/salesman/${sid}`);
    }
}
