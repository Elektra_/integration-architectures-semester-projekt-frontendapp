import { TestBed } from "@angular/core/testing";
import { EvaluationRecordService } from "./evaluation-record.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";

describe("EvaluationRecordService", () => {
    let service: EvaluationRecordService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        });
        service = TestBed.inject(EvaluationRecordService);
    });

    it("should be created", () => {
        expect(service).toBeTruthy();
    });
});
