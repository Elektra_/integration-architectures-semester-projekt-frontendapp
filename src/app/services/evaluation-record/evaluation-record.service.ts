import { Injectable } from "@angular/core";

import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { EvaluationRecord } from "../../interfaces";

@Injectable({
    providedIn: "root",
})
export class EvaluationRecordService {
    selectedEvaluationRecord: EvaluationRecord;
    constructor(private httpClient: HttpClient) {}

    /** GET: get evaluationRecords from one salesman from the server */
    getEvaluationRecords(sid: string): Observable<EvaluationRecord[]> {
        return this.httpClient.get<EvaluationRecord[]>(`${environment.backendApiUrl}/evaluation-records/${sid}`);
    }

    /** GET: get one evaluationRecord from one salesman from the server */
    getEvaluationRecord(sid: string, evaluationRecordId: string): Observable<EvaluationRecord> {
        return this.httpClient.get<EvaluationRecord>(
            `${environment.backendApiUrl}/evaluation-records/${sid}/${evaluationRecordId}`,
        );
    }

    /** POST: add a new evaluationRecord to the server */
    addEvaluationRecord(year: string, salesmanId: string, department: string) {
        return this.httpClient.post(`${environment.backendApiUrl}/evaluation-records/`, {
            year,
            salesmanId,
            department,
        });
    }

    /** PUT: update the evaluationRecord on the server */
    updateEvaluationRecord(evaluationRecord: EvaluationRecord): Observable<EvaluationRecord> {
        return this.httpClient.put<EvaluationRecord>(
            `${environment.backendApiUrl}/evaluation-records/${evaluationRecord._id}`,
            evaluationRecord,
        );
    }

    /** DELETE: delete the evaluation record from the server */
    deleteEvaluationRecord(evaluationRecord: EvaluationRecord | number): Observable<EvaluationRecord> {
        const _id = typeof evaluationRecord === "number" ? evaluationRecord : evaluationRecord._id;
        return this.httpClient.delete<EvaluationRecord>(`${environment.backendApiUrl}/evaluation-records/${_id}`);
    }
}
