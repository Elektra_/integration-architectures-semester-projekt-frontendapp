import { Inject, Injectable } from "@angular/core";

@Injectable({ providedIn: "root" })
export default class TokenService {
    constructor(@Inject("PersistentStorage") protected readonly storageProvider: Storage) {}

    getAccessToken(): string | null {
        return this.storageProvider.getItem("accessToken");
    }

    setAccessToken(accessToken: string) {
        return this.storageProvider.setItem("accessToken", accessToken);
    }

    deleteAccessToken() {
        return this.storageProvider.removeItem("accessToken");
    }
}
