import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Injectable } from "@angular/core";
import TokenService from "./token.service";
import { catchError } from "rxjs/operators";
import { Router } from "@angular/router";

/**
 * Adds the authorization token to the request object.
 */
@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
    constructor(protected readonly tokenService: TokenService, protected readonly router: Router) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // check if its even possible to add the authorization token
        const accessToken = this.tokenService.getAccessToken();
        if (accessToken) {
            const clonedRequest = req.clone({
                headers: req.headers.append("Authorization", `Bearer ${this.tokenService.getAccessToken()}`),
            });
            // Pass the cloned request instead of the original request to the next handle
            return next.handle(clonedRequest);
        }
        return next.handle(req).pipe(
            catchError((error: HttpErrorResponse) => {
                if (error.status === 401 || error.status === 403) {
                    // the user not authorized or the route was forbidden.
                    // since we only have a single login area, we will redirect him to the login screen
                    this.router.navigate(["/login"]);
                }
                // forward the error to do anything else.
                return throwError(error);
            }),
        );
    }
}
