import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../../environments/environment";
import TokenService from "./token.service";
import { mergeMap, tap } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

@Injectable({ providedIn: "root" })
export class AuthenticationService implements OnInit {
    isLoggedIn: boolean = false;
    public currentUser: { username: string; id: string; department: string } | null;

    constructor(private http: HttpClient, protected readonly tokenService: TokenService) {
        this.currentUser = null;
    }

    sync() {
        return this.http
            .get<{ username: string; id: string; department: string }>(`${environment.backendApiUrl}/auth`)
            .pipe(
                tap((data) => {
                    this.currentUser = data;
                    this.isLoggedIn = true;
                }),
            );
    }

    login(username, password) {
        return this.http
            .post<{ accessToken: string }>(`${environment.backendApiUrl}/auth/login`, { username, password })
            .pipe(
                tap((data) => {
                    this.tokenService.setAccessToken(data.accessToken);
                }),
                mergeMap(() => {
                    return this.sync();
                }),
            );
    }

    register(username: string, password: string, department: string) {
        return this.http
            .post<{ accessToken: string }>(`${environment.backendApiUrl}/auth/register`, {
                username,
                password,
                department,
            })
            .pipe(
                tap((data) => {
                    this.tokenService.setAccessToken(data.accessToken);
                }),
                mergeMap(() => {
                    return this.sync();
                }),
            );
    }

    public getCurrentUser(): Promise<{ username: string; id: string; department: string }> {
        return new Promise((resolve) => {
            if (!this.currentUser) {
                return this.sync().subscribe((user) => {
                    resolve(user);
                });
            }
            resolve(this.currentUser);
        });
    }

    logout(): void {
        this.currentUser = null;
        this.isLoggedIn = false;
        this.tokenService.deleteAccessToken();
    }

    ngOnInit(): void {
        if (this.tokenService.getAccessToken()) this.sync();
    }
}
