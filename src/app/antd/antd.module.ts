import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NzTableModule } from "ng-zorro-antd/table";
import { NzFormModule } from "ng-zorro-antd/form";
import { NzButtonModule } from "ng-zorro-antd/button";
import { NzCheckboxModule } from "ng-zorro-antd/checkbox";
import { NzInputModule } from "ng-zorro-antd/input";
import { NzPageHeaderModule } from "ng-zorro-antd/page-header";
import { NzSelectModule } from "ng-zorro-antd/select";
import { NzIconModule } from "ng-zorro-antd/icon";
import { NzMessageModule } from "ng-zorro-antd/message";

/**
 * Just wraps the antd component library and exports them.
 */
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        NzMessageModule,
        NzTableModule,
        NzFormModule,
        NzButtonModule,
        NzCheckboxModule,
        NzInputModule,
        NzPageHeaderModule,
        NzSelectModule,
        NzIconModule,
    ],
    exports: [
        NzTableModule,
        NzFormModule,
        NzButtonModule,
        NzCheckboxModule,
        NzInputModule,
        NzPageHeaderModule,
        NzSelectModule,
        NzIconModule,
    ],
})
export class AntdModule {}
