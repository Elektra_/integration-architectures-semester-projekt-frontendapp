import { Component, OnInit } from "@angular/core";
import { EvaluationRecordService } from "../../services/evaluation-record/evaluation-record.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SalesmanService } from "../../services/salesman/salesman.service";
import { AuthenticationService } from "../../services/auth/authentication.service";
import { Department, EvaluationRecord, Salesman } from "../../interfaces";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Location } from "@angular/common";

@Component({
    selector: "app-evaluation-records",
    templateUrl: "./evaluation-records.component.html",
    styleUrls: ["./evaluation-records.component.scss"],
})
export class EvaluationRecordsComponent implements OnInit {
    evaluationRecords: EvaluationRecord[];
    selectedEvaluationRecord: EvaluationRecord;
    salesman: Salesman;
    evaluationRecord: EvaluationRecord;
    yearForm: FormGroup;
    department: string;

    constructor(
        private evaluationRecordService: EvaluationRecordService,
        private route: ActivatedRoute,
        private salesmanService: SalesmanService,
        private router: Router,
        protected readonly formBuilder: FormBuilder,
        private authService: AuthenticationService,
        private location: Location,
    ) {}

    async ngOnInit(): Promise<void> {
        await this.getCurrentUser();

        this.yearForm = this.formBuilder.group({
            year: [null, Validators.required],
        });
        if (this.salesmanService.selectedSalesman) {
            this.salesman = this.salesmanService.selectedSalesman;
        } else {
            this.getSalesman();
        }

        this.getEvaluationRecords();
    }

    getSalesman(): void {
        const sid = this.route.snapshot.paramMap.get("sid");
        this.salesmanService.showSalesman(sid).subscribe((data) => {
            this.salesman = data;
        });
    }

    getEvaluationRecords(): void {
        const sid = this.route.snapshot.params["sid"];
        this.evaluationRecordService.getEvaluationRecords(sid).subscribe((data) => {
            this.evaluationRecords = data;
        });
    }

    onSubmit(): void {
        const year = this.yearForm.controls.year.value;
        if (!year) {
            return;
        }
        this.evaluationRecordService.addEvaluationRecord(year, this.salesman.code, this.salesman.unit).subscribe(() => {
            return this.router.navigate([`/evaluation-records/${this.salesman.code}`]);
        });
        window.location.reload();
    }

    deleteEvaluationRecord(evaluationRecord: EvaluationRecord): void {
        this.evaluationRecords = this.evaluationRecords.filter((e) => e !== evaluationRecord);
        this.evaluationRecordService.deleteEvaluationRecord(evaluationRecord).subscribe();
    }

    async getCurrentUser() {
        const data = await this.authService.getCurrentUser();
        this.department = data.department;
    }

    userIsSalesmanOrCEO(): boolean {
        return this.department === Department.Sales || this.department === Department.CEO;
    }

    onLinkPress(record: EvaluationRecord) {
        this.selectedEvaluationRecord = record;
        this.router.navigate([`/evaluation-records/${this.salesman.code}/edit/${record._id}`]);
    }

    goBack(): void {
        this.location.back();
    }
}
