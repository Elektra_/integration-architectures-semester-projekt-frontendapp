import { ComponentFixture, TestBed } from "@angular/core/testing";

import { EvaluationRecordsComponent } from "./evaluation-records.component";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { RouterTestingModule } from "@angular/router/testing";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import _PersistentStorageMock from "../../mocks/_PersistentStorageMock";
import { AntdModule } from "../../antd/antd.module";

describe("EvaluationRecordsComponent", () => {
    let component: EvaluationRecordsComponent;
    let fixture: ComponentFixture<EvaluationRecordsComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterTestingModule, AntdModule, ReactiveFormsModule, FormsModule],
            providers: [{ provide: "PersistentStorage", useClass: _PersistentStorageMock }],
            declarations: [EvaluationRecordsComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EvaluationRecordsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
