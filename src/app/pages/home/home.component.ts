import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SalesmanService } from "../../services/salesman/salesman.service";
import { Department, KPI, Salesman } from "../../interfaces";
import { AuthenticationService } from "../../services/auth/authentication.service";
import { KpiService } from "../../services/kpi/kpi.service";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
    salesmans: Salesman[];
    kpis: KPI[];
    department: string;
    name: string;
    id: string;

    editableKPI = "";
    editableTargetValue = "";

    constructor(
        private router: Router,
        private salesmanService: SalesmanService,
        private authService: AuthenticationService,
        private readonly kpiService: KpiService,
    ) {}

    async ngOnInit(): Promise<void> {
        await this.getCurrentUser();
        if (this.userIsSalesman() == true) {
            this.salesmanService.getSalesman().subscribe((data) => {
                this.salesmans = data.filter((salesman) => salesman.fullName == this.name);
            });
        } else {
            this.salesmanService.getSalesman().subscribe((data) => {
                this.salesmans = data;
            });
            this.kpiService.getAll().subscribe((data) => (this.kpis = data));
        }
    }

    async onLinkPress(salesman: Salesman) {
        this.salesmanService.selectedSalesman = salesman;
        await this.router.navigate([`/evaluation-records/${salesman.code}`]);
    }

    async getCurrentUser() {
        const data = await this.authService.getCurrentUser();
        this.name = data.username;
        this.department = data.department;
        this.id = data.id;
    }

    addKPI() {
        this.kpiService.create({ kpi: this.editableKPI, targetValue: this.editableTargetValue }).subscribe((data) => {
            this.kpis = this.kpis.concat(data);
            this.editableKPI = "";
            this.editableTargetValue = "";
        });
    }

    deleteKPI(id: string) {
        this.kpiService.delete(id).subscribe((data) => {
            this.kpis = this.kpis.filter((kpi) => kpi._id !== id);
        });
    }

    userIsSalesman(): boolean {
        return this.department === Department.Sales;
    }

    userIsCEO(): boolean {
        return this.department === Department.CEO;
    }
}
