import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "../../services/auth/authentication.service";
import { NzMessageService } from "ng-zorro-antd/message";

@Component({ templateUrl: "login.component.html" })
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    returnUrl: "localhost:4200/home";

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private messageService: NzMessageService,
        private authenticationService: AuthenticationService, // private alertService: AlertService
    ) {}

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            username: [null, Validators.required],
            password: [null, Validators.required],
        });

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams.returnUrl || "/";
    }

    onSubmit(): void {
        if (this.loginForm.invalid) {
            return;
        }
        this.authenticationService.login(this.f.username.value, this.f.password.value).subscribe(
            () => {
                return this.router.navigate(["/home"]);
            },
            () => {
                this.messageService.error("Please double check your credentials.");
            },
        );
    }

    onRegisterButtonClick() {
        return this.router.navigate(["/register"]);
    }
}
