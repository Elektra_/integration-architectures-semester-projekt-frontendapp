import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { EvaluationRecordService } from "../../services/evaluation-record/evaluation-record.service";
import { Location } from "@angular/common";
import { SalesmanService } from "../../services/salesman/salesman.service";
import { Department, EvaluationRecord, Salesman, SocialEvaluation } from "../../interfaces";
import { AuthenticationService } from "../../services/auth/authentication.service";
import BonusCalculator from "../../utils/BonusCalculator";
import { NzMessageService } from "ng-zorro-antd/message";

@Component({
    selector: "app-edit-evaluation-record",
    templateUrl: "./edit-evaluation-record.component.html",
    styleUrls: ["./edit-evaluation-record.component.css"],
})
export class EditEvaluationRecordComponent implements OnInit {
    evaluationRecord: EvaluationRecord;
    salesman: Salesman;
    department: string;

    constructor(
        private message: NzMessageService,
        private route: ActivatedRoute,
        private evaluationRecordService: EvaluationRecordService,
        private salesmanService: SalesmanService,
        private location: Location,
        private authService: AuthenticationService,
    ) {}

    calculateSocialBonus(element: SocialEvaluation) {
        return BonusCalculator.socialEvaluationBonus(+element.actualValue, +element.targetValue ?? 0);
    }

    calculateTotalBonus() {
        return this.calculateTotalSocialBonus() + this.calculateTotalOrderBonus();
    }

    calculateTotalOrderBonus() {
        return this.evaluationRecord.orderEvaluation.reduce((a, b) => a + (b.bonus ?? 0), 0);
    }

    calculateTotalSocialBonus() {
        return this.evaluationRecord.socialEvaluation.reduce((a, b) => a + (b.bonus ?? 0), 0);
    }

    async ngOnInit(): Promise<void> {
        this.department = (await this.authService.getCurrentUser()).department;
        if (!this.salesmanService.selectedSalesman) {
            this.salesman = await this.getSalesman();
        } else {
            this.salesman = this.salesmanService.selectedSalesman;
        }
        this.evaluationRecord = await this.getEvaluationRecord();
    }

    getSalesman(): Promise<Salesman> {
        return new Promise((resolve) => {
            const sid = this.route.snapshot.paramMap.get("sid");
            this.salesmanService.showSalesman(sid).subscribe((data) => {
                resolve(data);
            });
        });
    }

    getEvaluationRecord(): Promise<EvaluationRecord> {
        return new Promise((resolve) => {
            const sid = this.route.snapshot.paramMap.get("sid");
            const evaluationRecordId = this.route.snapshot.paramMap.get("evaluationRecordId");
            this.evaluationRecordService.getEvaluationRecord(sid, evaluationRecordId).subscribe((data) => {
                resolve(data);
            });
        });
    }

    goBack(): void {
        this.location.back();
    }

    save(): void {
        this.evaluationRecordService
            .updateEvaluationRecord({ ...this.evaluationRecord, bonus: this.calculateTotalBonus() })
            .subscribe((data) => {
                this.evaluationRecord = data;
                this.message.success("Successfully saved");
            });
    }

    confirmRecord(): void {
        if (this.department === Department.HR) {
            this.evaluationRecord.isApprovedByHR = true;
        } else if (this.department === Department.CEO) {
            this.evaluationRecord.isApprovedByCEO = true;
        } else {
            this.evaluationRecord.isApprovedBySalesman = true;
        }
        this.evaluationRecordService
            .updateEvaluationRecord({ ...this.evaluationRecord, bonus: this.calculateTotalBonus() })
            .subscribe(() => this.goBack());
    }

    userIsHR(): boolean {
        return this.department === Department.HR;
    }
}
