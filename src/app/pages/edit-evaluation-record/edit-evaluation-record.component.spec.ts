import { ComponentFixture, getTestBed, TestBed } from "@angular/core/testing";

import { EditEvaluationRecordComponent } from "./edit-evaluation-record.component";
import { NzMessageModule, NzMessageService } from "ng-zorro-antd/message";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import _PersistentStorageMock from "../../mocks/_PersistentStorageMock";
import { AntdModule } from "../../antd/antd.module";
import { ActivatedRoute } from "@angular/router";

describe("EditEvaluationRecordComponent", () => {
    let component: EditEvaluationRecordComponent;
    let fixture: ComponentFixture<EditEvaluationRecordComponent>;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [NzMessageModule, RouterTestingModule, AntdModule, HttpClientTestingModule],
            providers: [
                { provide: "PersistentStorage", useClass: _PersistentStorageMock },
                {
                    provide: ActivatedRoute,
                    useValue: {
                        snapshot: {
                            paramMap: {
                                get(name: string) {
                                    return name;
                                },
                            },
                        },
                    },
                },
            ],
            declarations: [EditEvaluationRecordComponent],
        }).compileComponents();
        const injector = getTestBed();
        httpMock = injector.get(HttpTestingController);
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(EditEvaluationRecordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
});
