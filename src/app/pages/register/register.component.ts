import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthenticationService } from "../../services/auth/authentication.service";
import { NzMessageService } from "ng-zorro-antd/message";

@Component({
    selector: "app-register",
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.css"],
})
export class RegisterComponent implements OnInit {
    registerForm: FormGroup;

    constructor(
        private messageService: NzMessageService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService, // private alertService: AlertService
    ) {}

    // convenience getter for easy access to form fields
    get f() {
        return this.registerForm.controls;
    }

    ngOnInit(): void {
        this.registerForm = this.formBuilder.group({
            username: [null, Validators.required],
            password: [null, Validators.required],
            department: [null, Validators.required],
        });
    }

    onSubmit(): void {
        if (this.registerForm.invalid) {
            this.messageService.warning("Please double check the form.");
            return;
        }
        this.authenticationService
            .register(this.f.username.value, this.f.password.value, this.f.department.value)
            .subscribe(() => {
                // the token is already stored. We can redirect the user to the home route.
                return this.router.navigate(["/home"]);
            });
    }

    onLoginButtonClick() {
        return this.router.navigate(["/login"]);
    }
}
