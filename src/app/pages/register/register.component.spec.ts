import { ComponentFixture, TestBed } from "@angular/core/testing";

import { RegisterComponent } from "./register.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterTestingModule } from "@angular/router/testing";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import _PersistentStorageMock from "../../mocks/_PersistentStorageMock";
import { AntdModule } from "../../antd/antd.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NzMessageService } from "ng-zorro-antd/message";
import sinon from "sinon/pkg/sinon-esm.js";
import { AuthenticationService } from "../../services/auth/authentication.service";
import { of } from "rxjs";
import { Router } from "@angular/router";
import { HomeComponent } from "../home/home.component";

describe("RegisterComponent", () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    let messageService: NzMessageService;
    let httpMock: HttpTestingController;
    let authenticationService: AuthenticationService;
    let router: Router;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([{ path: "home", component: HomeComponent }]),
                HttpClientTestingModule,
                AntdModule,
            ],
            providers: [{ provide: "PersistentStorage", useClass: _PersistentStorageMock }],
            declarations: [RegisterComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        messageService = TestBed.get(NzMessageService);
        httpMock = TestBed.get(HttpTestingController);
        authenticationService = TestBed.get(AuthenticationService);
        router = TestBed.get(Router);
        fixture.detectChanges();
    });

    it("should show a warning toast, if the user tries to register without filling out the form.", () => {
        const mock = sinon.mock(messageService);
        mock.expects("warning").withArgs("Please double check the form.");
        component.onSubmit();
        expect(mock.verify()).toBeTrue();
    });

    it("should call the register route, when the submit is called and the form is correct", () => {
        const username = "Test";
        const password = "secret";
        const department = "Sales";
        component.registerForm.controls["username"].setValue(username);
        component.registerForm.controls["password"].setValue(password);
        component.registerForm.controls["department"].setValue(department);
        const mock = sinon.mock(authenticationService);
        mock.expects("register").withArgs(username, password, department).returns(of({}));
        component.onSubmit();
        expect(mock.verify()).toBeTrue();
    });
    it("should navigate to the home router, after submitting the form", () => {
        const username = "Test";
        const password = "secret";
        const department = "Sales";
        component.registerForm.controls["username"].setValue(username);
        component.registerForm.controls["password"].setValue(password);
        component.registerForm.controls["department"].setValue(department);
        sinon.mock(authenticationService).expects("register").withArgs(username, password, department).returns(of({}));
        const mock = sinon.mock(router).expects("navigate").withArgs(["/home"]);
        component.onSubmit();
        expect(mock.verify()).toBeTrue();
    });
});
